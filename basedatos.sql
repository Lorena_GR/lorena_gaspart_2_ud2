CREATE DATABASE if not exists ropa_ceremonia;
USE ropa_ceremonia;

create table if not exists diseniador(
iddiseniador int auto_increment primary key,
nombre varchar(20) not null,
apellido varchar(20) not null,
fechanacimiento date,
genero varchar(20) not null
);

create table if not exists vestido(
idvestido int auto_increment primary key,
nombre varchar(50) not null unique ,
estacion varchar(20) not null,
color varchar(20) not null,
talla int not null,
iddiseniador int not null,
marca varchar (30) not null,
precio float not null,
fechacompra date,
fechaentrega date
);

create table if not exists traje(
idtraje int auto_increment primary key,
nombre varchar(50) not null unique,
estacion varchar(20) not null,
color varchar(20) not null,
talla int not null,
iddiseniador int not null,
marca varchar (30) not null,
precio float not null,
fechacompra date,
fechaentrega date,
);

alter table vestido
add foreign key (iddiseniador) references diseniador (iddiseniador)
;

alter table traje
add foreign key (iddiseniador) references diseniador (iddiseniador)
;
--
delimiter ||
create function existeNombreDiseniador(f_nombre varchar (50))
returns bit
begin
    declare i int;
    set i = 0;
    while(i<(select max(iddiseniador)from diseniador)) do
    if ((select concat(apellido, ", ", nombre) from diseniador where iddiseniador = (i + 1)) like f_nombre) then return 1;
    end if;
    set i = i + 1;
    end while;
    return 0;
    end; ||
delimiter;
--
delimiter ||
create function existeNombreVestido(f_nombre varchar (50))
returns bit
begin
    declare i int;
    set i = 0;
    while(i<(select max(idvestido)from vestido)) do
    if ((select nombre from vestido where idvestido = (i + 1)) like f_nombre) then return 1;
    end if;
    set i = i + 1;
    end while;
    return 0;
    end; ||
delimiter;
--
delimiter ||
create function existeNombreTraje(f_nombre varchar (50))
returns bit
begin
    declare i int;
    set i = 0;
    while(i<(select max(idtraje)from traje)) do
    if ((select nombre from traje where idtraje = (i + 1)) like f_nombre) then return 1;
    end if;
    set i = i + 1;
    end while;
    return 0;
    end; ||