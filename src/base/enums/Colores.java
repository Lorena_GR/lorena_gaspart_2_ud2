package base.enums;

/**
 * Esta clase creara los diferentes elementos del combo box de color
 */
public enum Colores {

    NEGRO("Negro"),
    AZUL("Azul"),
    MARRON("Marrón"),
    GRIS("Gris"),
    VERDE("Verde"),
    NARANJA("Naranja"),
    ROSA("Rosa"),
    PURPURA("Púrpura"),
    ROJO("Rojo"),
    BLANCO("Blanco"),
    AMARILLO("Amarillo");
    private String color;

    /**
     * Constructor de la clase enum de color. Que inicia el atributo color.
     * @param color
     */
    Colores(String color){
        this.color=color;
    }

    public String getColor() {
        return color;
    }
}
