package base.enums;
/**
 * Esta clase creara los diferentes elementos del combo box de la marca de los trajes
 */
public enum MarcasTrajes {
    MRPORTER("Mr Porter"),
    HUGOBOSS("Hugo Boss"),
    ZEGNA("Ermenegildo Zegna"),
    ETRO("Etro"),
    BURBERRY("Burberry"),
    BOGGIMILANO("Boggi Milano"),
    SILBON("Silbon"),
    BROOKBROTHERS("Brook Brothers"),
    SUITMAN("Suitman"),
    PUROEGO("Puro Ego");

    private String marca;

    /**
     * Constructor que inicia la marca
     * @param marca
     */
    MarcasTrajes(String marca){
        this.marca=marca;
    }

    public String getMarca() {
        return marca;
    }
}
