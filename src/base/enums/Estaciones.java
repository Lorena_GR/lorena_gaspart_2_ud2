package base.enums;
/**
 * Esta clase creara los diferentes elementos del combo box de estacion
 */
public enum Estaciones {

    PRIMAVERA("Primavera"),
    VERANO("Verano"),
    OTONO("Otoño"),
    INVIERNO("Invierno");


    private String estacion;

    /**
     * Constructor que inicia el atributo estacion
     * @param estacion
     */
    Estaciones(String estacion){
        this.estacion=estacion;
    }

    public String getEstacion() {
        return estacion;
    }
}
