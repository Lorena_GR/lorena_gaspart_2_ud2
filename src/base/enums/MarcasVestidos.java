package base.enums;
/**
 * Esta clase creara los diferentes elementos del combo box de la marca de los trajes
 */
public enum MarcasVestidos {
    BGOANDME("BGO and Me"),
    LOLALI("Lola Li"),
    MATILDECANO("Matilde Cano"),
    LADYPIPA("Lady Pipa"),
    BLANCASPINA("BlancaSpina"),
    VOGANA("Vogana"),
    ATELIER("Atelier"),
    ROSACLARA("Rosa Clara"),
    BIMANI("Bimani 13"),
    VANDERWILDE("VanderWilde");

    private String marca;
    /**
     * Constructor que inicia la marca
     * @param marca
     */
    MarcasVestidos(String marca){
        this.marca=marca;
    }

    public String getMarca() {
        return marca;
    }
}
