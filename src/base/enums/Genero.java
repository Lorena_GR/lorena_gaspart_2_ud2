package base.enums;

/**
 * Esta clase creara los diferentes elementos del combo box de genero
 */
public enum Genero {
    MASCULINO("Masculino"),
    FEMENINO("Femenino"),
    SECRETO("Secreto");

    private String genero;

    /**
     * Constructor que inicia el atributo genero
     * @param genero
     */
    Genero(String genero){
        this.genero=genero;
    }

    public String getGenero() {
        return genero;
    }
}
