package gui;

import util.Util;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Vector;

/**
 * Clase controlador con las diferentes implementaciones.
 */
public class Controlador implements ActionListener, ItemListener, ListSelectionListener, WindowListener {
    private Modelo modelo;
    private Vista vista;
    boolean refrescar;

    /**
     * Controlador que inicia el modelo y la vista y llama a los diferentes metodos
     * @param vista
     * @param modelo
     */
    public Controlador(Vista vista, Modelo modelo){
        this.modelo = modelo;
        this.vista = vista;
        modelo.conectar();
        setOptions();
        addActionListeners(this);
        addItemListeneres(this);
        addWindowListeners(this);
        refrescarTodo();
    }


    /**
     * Metodo que llama a todos los metodos de refrescar y pone refrescar en false
     */
    private void refrescarTodo(){
        refrescarDiseniador();
        refrescarVestido();
        refrescarTraje();
        refrescar = false;
    }
    private void setDataVector(ResultSet rs, int columnCount, Vector<Vector<Object>> data) throws SQLException{
        while (rs.next()){
            Vector<Object> vector = new Vector<>();
            for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++){
                vector.add(rs.getObject(columnIndex));
            }
            data.add(vector);
        }
    }

    /**
     * Metodo que refresca el traje
     */
    private void refrescarTraje(){
        try {
            vista.tablaTraje.setModel(construirTableModeloTraje(modelo.consultarTraje()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private DefaultTableModel construirTableModeloTraje(ResultSet rs) throws SQLException{
        ResultSetMetaData metaData = rs.getMetaData();
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++){
            columnNames.add(metaData.getColumnName(column));
        }
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);
        vista.dtmTraje.setDataVector(data, columnNames);
        return vista.dtmTraje;

    }

    /**
     * Metodo que refresca los vestidos
     */
    private void refrescarVestido() {
        try {
            vista.tablaVestido.setModel(construirTableModeloVestido(modelo.consultarVestido()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private DefaultTableModel construirTableModeloVestido(ResultSet rs) throws SQLException{
        ResultSetMetaData metaData = rs.getMetaData();
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++){
            columnNames.add(metaData.getColumnName(column));
        }
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);
        vista.dtmVestido.setDataVector(data, columnNames);
        return vista.dtmVestido;

    }

    /**
     * Metodo que refresca el diseñador
     */
    private void refrescarDiseniador() {
        try {
            vista.tablaDisenador.setModel(construirTableModeloDiseniador(modelo.consultarDiseniador()));
            vista.comboDiseniadorVestido.removeAllItems();
            vista.comboDiseniadorTraje.removeAllItems();
            for (int i = 0; i < vista.dtmDisenador.getRowCount(); i++){
                vista.comboDiseniadorTraje.addItem(vista.dtmDisenador.getValueAt(i, 0)+ " - " + vista.dtmDisenador.getValueAt(i, 2) + ", " + vista.dtmDisenador.getValueAt(i, 1));
                vista.comboDiseniadorVestido.addItem(vista.dtmDisenador.getValueAt(i, 0)+ " - " + vista.dtmDisenador.getValueAt(i, 2) + ", " + vista.dtmDisenador.getValueAt(i, 1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private DefaultTableModel construirTableModeloDiseniador(ResultSet rs) throws SQLException{
        ResultSetMetaData metaData = rs.getMetaData();
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++){
            columnNames.add(metaData.getColumnName(column));
        }
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);
        vista.dtmDisenador.setDataVector(data, columnNames);
        return vista.dtmDisenador;

    }

    private void setOptions() {

    }

    /**
     * Metodo que añade los action listeners y les da un nombre para llamarlo en el switch
     * @param listener
     */
    private void addActionListeners(ActionListener listener){
        vista.tablaDisenador.getSelectionModel().addListSelectionListener(this);
        vista.tablaTraje.getSelectionModel().addListSelectionListener(this);
        vista.tablaVestido.getSelectionModel().addListSelectionListener(this);
        // DISEÑADOR
        vista.btnAnyadirDisen.addActionListener(listener);
        vista.btnAnyadirDisen.setActionCommand("anadirDisen");
        vista.btnEliminarDisen.addActionListener(listener);
        vista.btnEliminarDisen.setActionCommand("eliminarDisen");
        vista.btnModificarDisen.addActionListener(listener);
        vista.btnModificarDisen.setActionCommand("modificarDisen");
        vista.btnAnyadirDisen.addActionListener(listener);
        vista.btnBuscarDisen.addActionListener(listener);
        vista.btnBuscarDisen.setActionCommand("buscarDisen");
        // VESTIDO
        vista.btnAnyadirVestido.setActionCommand("anadirVestido");
        vista.btnAnyadirVestido.addActionListener(listener);
        vista.btnEliminarVestido.setActionCommand("eliminarVestido");
        vista.btnEliminarVestido.addActionListener(listener);
        vista.btnModificarVestido.setActionCommand("modificarVestido");
        vista.btnModificarVestido.addActionListener(listener);
        //TRAJE
        vista.btnAnyadirTraje.addActionListener(listener);
        vista.btnAnyadirTraje.setActionCommand("anadirTraje");
        vista.btnEliminarTraje.addActionListener(listener);
        vista.btnEliminarTraje.setActionCommand("eliminarTraje");
        vista.btnModificarTraje.addActionListener(listener);
        vista.btnModificarTraje.setActionCommand("modificarTraje");
        // MENU Y OPTION DIALOG
        vista.optionDialog.btnOpcionesGuardar.addActionListener(listener);
        vista.itemOpciones.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);
        vista.itemDesconectar.addActionListener(listener);
        vista.btnValidate.addActionListener(listener);

    }

    /**
     * Crea el windowListener
     * @param listener
     */
    private void addWindowListeners (WindowListener listener){
        vista.addWindowListener(listener);
    }

    private void addItemListeneres(Controlador controlador){

    }

    /**
     * Este metodo segun que linea de una tabla hagas click. Te rellenara los diferentes campos con su información.
     * @param e
     */
    public void valueChanged(ListSelectionEvent e){
        if(e.getValueIsAdjusting() && !((ListSelectionModel) e.getSource()).isSelectionEmpty()){
            if (e.getSource().equals(vista.tablaDisenador.getSelectionModel())){
                int row = vista.tablaDisenador.getSelectedRow();
                vista.txtNombreDisen.setText(String.valueOf(vista.tablaDisenador.getValueAt(row, 1)));
                vista.txtApellido.setText(String.valueOf(vista.tablaDisenador.getValueAt(row, 2)));
                vista.dateFechaNac.setDate((Date.valueOf(String.valueOf(vista.tablaDisenador.getValueAt(row, 3)))).toLocalDate());
                vista.comboGenero.setSelectedItem(String.valueOf(vista.tablaDisenador.getValueAt(row,4)));
            } else if(e.getSource().equals(vista.tablaVestido.getSelectionModel())){
                int row = vista.tablaVestido.getSelectedRow();
                vista.txtNombreVestido.setText(String.valueOf(vista.tablaVestido.getValueAt(row, 1)));
                vista.comboEstacionVestido.setSelectedItem(String.valueOf(vista.tablaVestido.getValueAt(row,2)));
                vista.comboColorVestido.setSelectedItem(String.valueOf(vista.tablaVestido.getValueAt(row, 3)));
                vista.sliderTallaVestido.setValue(Integer.parseInt(String.valueOf(vista.tablaVestido.getValueAt(row, 4))));
                vista.comboDiseniadorVestido.setSelectedItem(String.valueOf(vista.tablaVestido.getValueAt(row, 5)));
                vista.comboMarcaVestido.setSelectedItem(String.valueOf(vista.tablaVestido.getValueAt(row, 6)));
                vista.txtPrecioVestido.setText(String.valueOf(Double.parseDouble(String.valueOf(vista.tablaVestido.getValueAt(row, 7)))));
                vista.dateFechaCompraVestido.setDate(LocalDate.parse(String.valueOf(vista.tablaVestido.getValueAt(row, 8))));
                vista.dateFechaEntregaVestido.setDate(LocalDate.parse(String.valueOf(vista.tablaVestido.getValueAt(row, 9))));
            } else if(e.getSource().equals(vista.tablaTraje.getSelectionModel())){
                int row = vista.tablaTraje.getSelectedRow();
                vista.txtNombreTraje.setText(String.valueOf(vista.tablaTraje.getValueAt(row, 1)));
                vista.comboEstacionTraje.setSelectedItem(String.valueOf(vista.tablaTraje.getValueAt(row,2)));
                vista.comboColorTraje.setSelectedItem(String.valueOf(vista.tablaTraje.getValueAt(row, 3)));
                vista.sliderTallaTraje.setValue(Integer.parseInt(String.valueOf(vista.tablaTraje.getValueAt(row, 4))));
                vista.comboDiseniadorTraje.setSelectedItem(String.valueOf(vista.tablaTraje.getValueAt(row, 5)));
                vista.comboMarcaTraje.setSelectedItem(String.valueOf(vista.tablaTraje.getValueAt(row, 6)));
                vista.txtPrecioTraje.setText(String.valueOf(Double.valueOf(String.valueOf(vista.tablaTraje.getValueAt(row, 7)))));
                vista.dateFechaCompraTraje.setDate(LocalDate.parse(String.valueOf(vista.tablaTraje.getValueAt(row, 8))));
                vista.dateFechaEntregaTraje.setDate(LocalDate.parse(String.valueOf(vista.tablaTraje.getValueAt(row, 9))));
            } else if(e.getValueIsAdjusting() && ((ListSelectionModel) e.getSource()).isSelectionEmpty() && !refrescar){
                if(e.getSource().equals(vista.tablaDisenador.getSelectionModel())){
                    borrarCamposDiseniador();
                } else if(e.getSource().equals(vista.tablaVestido.getSelectionModel())){
                    borrarCamposVestido();
                } else if(e.getSource().equals(vista.tablaTraje.getSelectionModel())){
                    borrarCamposTraje();
                }
            }
        }
    }

    /**
     * Metodo que lleva el switch de las diferentes acciones de las ventanas y tablas.
     * @param e
     */
    public void actionPerformed(ActionEvent e){
        String command = e.getActionCommand();
        switch (command){
            /**
             * Case conectar que llama al metodo conectar
             */
            case "Conectar":
                modelo.conectar();
                break;
            /**
             * Caso opciones que muestra el cuadro de que pide la contraseña para acceder a la ventana opciones
             */
            case "Opciones":
                vista.adminPasswordDialog.setVisible(true);
                break;
            /**
             * Caso que llama al metodo desconectar
             */
            case "Desconectar":
                modelo.desconectar();
                break;
            /**
             * Caso que cierra la aplicacion
             */
            case "Salir":
                System.exit(0);
                break;
            /**
             * Caso que abre las opciones cuando la contraseña que te pide coincide con la contraseña guardada en config.properties te lleva a las opciones.
             * Y si fallas la contraseña te sale el mensaje de contraseña incorrecta
             */
            case "abrirOpciones":
                if(String.valueOf(vista.adminPassword.getPassword()).equals(modelo.getAdminPassword())){
                    vista.adminPassword.setText("");
                    vista.adminPasswordDialog.dispose();
                    vista.optionDialog.setVisible(true);
                }else {
                    Util.errorAlerta("La contraseña no es correcta");
                }
                break;
            /**
             * Al darle al boton guardar dentro de las opciones llamara al metodo setPropValues que guardara los los distintos textos para luego guardarlos en el archvio
             */
            case "Guardar":
                modelo.setPropValues(vista.optionDialog.tfIP.getText(),
                        vista.optionDialog.tfUsuario.getText(),
                        String.valueOf(vista.optionDialog.pfContr.getPassword()),
                        String.valueOf(vista.optionDialog.pfContraAdmin.getPassword()));
                vista.optionDialog.dispose();
                vista.dispose();
                new Controlador(new Vista(), new Modelo());
                break;
            /**
             * Caso que se usa para añadir el diseñador. Que si al comprobar los campos alguno esta vacio saldra un mensaje de error.
             * Si ya existe el diseñador dara un mensaje de que ya existe.
             * Y si esta bien insertara los datos en la base de datos
             */
            case "anadirDisen":
                    if (comprobarDiseniadorVacio()) {
                        Util.errorAlerta("Rellena todos los campos requeridos");
                        vista.tablaDisenador.clearSelection();
                    } else if (modelo.disenadorYaExiste(vista.txtNombreDisen.getText(), vista.txtApellido.getText())) {
                        Util.errorAlerta("Ese diseñador ya existe");
                        vista.tablaDisenador.clearSelection();
                    } else {
                        modelo.insertarDisenador(
                                vista.txtNombreDisen.getText(),
                                vista.txtApellido.getText(),
                                vista.dateFechaNac.getDate(),
                                String.valueOf(vista.comboGenero.getSelectedItem()));
                        refrescarDiseniador();
                    }
                    borrarCamposDiseniador();
                    break;
            /**
             * Caso que modificara el diseñador, si algun campo esta vacio saldra mensaje de error.
             * Si ninguno esta vacio entonces lo modificara
             * AL final refrescara la tabla para mostrar los valores y vaciara los campos
              */
            case "modificarDisen":
                if(comprobarDiseniadorVacio()){
                    Util.errorAlerta("Rellena todos los campos");
                    vista.tablaDisenador.clearSelection();
                } else {
                    modelo.modificarDisenador(
                            vista.txtNombreDisen.getText(),
                            vista.txtApellido.getText(),
                            vista.dateFechaNac.getDate(),
                            String.valueOf(vista.comboGenero.getSelectedItem()),
                            Integer.parseInt((String) vista.tablaDisenador.getValueAt(vista.tablaDisenador.getSelectedRow(), 0))
                    );

                }
                borrarCamposDiseniador();
                refrescarDiseniador();
                break;
            /**
             * Eliminara el diseñador seleccionado
             */
            case "eliminarDisen":
                modelo.eliminarDisenador(Integer.parseInt((String) vista.tablaDisenador.getValueAt(vista.tablaDisenador.getSelectedRow(), 0)));
                borrarCamposDiseniador();
                refrescarDiseniador();
                break;
            case "buscarDisen":
                modelo.buscarDiseniador((String) vista.tablaDisenador.getValueAt(vista.tablaDisenador.getSelectedRow(), 1));

                break;
            /**
             * Añade un vestido...
              */
            case "anadirVestido":
                if (comprobarVestidoVacio()) {
                    Util.errorAlerta("Rellena todos los campos requeridos");
                    vista.tablaVestido.clearSelection();
                } else if (modelo.nombreVestidoYaExiste(vista.txtNombreVestido.getText())) {
                    Util.errorAlerta("Ese vestido ya existe");
                    vista.tablaVestido.clearSelection();
                } else {
                    modelo.insertarVestido(
                            vista.txtNombreVestido.getText(),
                            String.valueOf(vista.comboEstacionVestido.getSelectedItem()),
                            String.valueOf(vista.comboColorVestido.getSelectedItem()),
                            Integer.parseInt(String.valueOf(vista.sliderTallaVestido.getValue())),
                            String.valueOf(vista.comboDiseniadorVestido.getSelectedItem()),
                            String.valueOf(vista.comboMarcaVestido.getSelectedItem()),
                            Float.parseFloat(String.valueOf(vista.txtPrecioVestido.getText())),
                            vista.dateFechaCompraVestido.getDate(),
                            vista.dateFechaEntregaVestido.getDate());
                }
                borrarCamposVestido();
                refrescarVestido();
                break;
            /**
             * Modifica un vestido
             */
            case "modificarVestido":
                if(comprobarVestidoVacio()){
                    Util.errorAlerta("Rellena todos los campos");
                    vista.tablaVestido.clearSelection();
                } else {
                    modelo.modificarVestido(
                            vista.txtNombreVestido.getText(),
                            String.valueOf(vista.comboEstacionVestido.getSelectedItem()),
                            String.valueOf(vista.comboColorVestido.getSelectedItem()),
                            Integer.parseInt(String.valueOf(vista.sliderTallaVestido.getValue())),
                            String.valueOf(vista.comboDiseniadorVestido.getSelectedItem()),
                            String.valueOf(vista.comboMarcaVestido.getSelectedItem()),
                            Float.parseFloat(String.valueOf(vista.txtPrecioVestido.getText())),
                            vista.dateFechaCompraVestido.getDate(),
                            vista.dateFechaEntregaVestido.getDate(),
                            Integer.parseInt((String) vista.tablaVestido.getValueAt(vista.tablaVestido.getSelectedRow(),0))
                    );
                }
                borrarCamposVestido();
                refrescarVestido();
                break;
            /**
             * Elimina un vestido
             */
            case "eliminarVestido":
                modelo.eliminarVestido((String) vista.tablaVestido.getValueAt(vista.tablaVestido.getSelectedRow(), 1));
                borrarCamposVestido();
                refrescarVestido();
                break;
            /**
             * Añade un traje
             */
            case "anadirTraje":
                if (comprobarTrajeVacio()) {
                    Util.errorAlerta("Rellena todos los campos requeridos");
                    vista.tablaTraje.clearSelection();
                } else if (modelo.nombreTrajeYaExiste(vista.txtNombreTraje.getText())) {
                    Util.errorAlerta("Ese traje ya existe");
                    vista.tablaTraje.clearSelection();
                } else {
                    modelo.insertarTraje(
                            vista.txtNombreTraje.getText(),
                            String.valueOf(vista.comboEstacionTraje.getSelectedItem()),
                            String.valueOf(vista.comboColorTraje.getSelectedItem()),
                            Integer.parseInt(String.valueOf(vista.sliderTallaTraje.getValue())),
                            String.valueOf(vista.comboDiseniadorTraje.getSelectedItem()),
                            String.valueOf(vista.comboMarcaTraje.getSelectedItem()),
                            Float.parseFloat(String.valueOf(vista.txtPrecioTraje.getText())),
                            vista.dateFechaCompraTraje.getDate(),
                            vista.dateFechaEntregaTraje.getDate());
                }
                borrarCamposTraje();
                refrescarTraje();
                break;
            /**
             * Modifica un traje
             */
            case "modificarTraje":
                if(comprobarTrajeVacio()){
                    Util.errorAlerta("Rellena todos los campos");
                    vista.tablaTraje.clearSelection();
                } else {
                    modelo.modificarTraje(
                            vista.txtNombreTraje.getText(),
                            String.valueOf(vista.comboEstacionTraje.getSelectedItem()),
                            String.valueOf(vista.comboColorTraje.getSelectedItem()),
                            Integer.parseInt(String.valueOf(vista.sliderTallaTraje.getValue())),
                            String.valueOf(vista.comboDiseniadorTraje.getSelectedItem()),
                            String.valueOf(vista.comboMarcaTraje.getSelectedItem()),
                            Float.parseFloat(String.valueOf(vista.txtPrecioTraje.getText())),
                            vista.dateFechaCompraTraje.getDate(),
                            vista.dateFechaEntregaTraje.getDate(),
                            Integer.parseInt((String) vista.tablaTraje.getValueAt(vista.tablaTraje.getSelectedRow(),0))
                    );
                }
                borrarCamposTraje();
                refrescarTraje();
                break;
            /**
             * Elimina un traje
             */
            case "eliminarTraje":
                modelo.eliminarTraje((String) vista.tablaTraje.getValueAt(vista.tablaTraje.getSelectedRow(), 1));
                borrarCamposTraje();
                refrescarTraje();
                break;
        }
    }

    /**
     * Metodo que limpia los campos de traje
     */
    private void borrarCamposTraje() {
        vista.txtNombreTraje.setText("");
        vista.comboEstacionTraje.setSelectedItem(-1);
        vista.comboColorTraje.setSelectedItem(-1);
        vista.sliderTallaTraje.setValue(30);
        vista.comboDiseniadorTraje.setSelectedItem(-1);
        vista.comboMarcaTraje.setSelectedItem(-1);
        vista.txtPrecioTraje.setText("");
        vista.dateFechaCompraTraje.setText("");
        vista.dateFechaEntregaTraje.setText("");
    }

    /**
     * Metodo que limpia los campos de vestido
     */
    private void borrarCamposVestido() {
        vista.txtNombreVestido.setText("");
        vista.comboEstacionVestido.setSelectedItem(-1);
        vista.comboColorVestido.setSelectedItem(-1);
        vista.sliderTallaVestido.setValue(30);
        vista.comboDiseniadorVestido.setSelectedItem(-1);
        vista.comboMarcaVestido.setSelectedItem(-1);
        vista.txtPrecioVestido.setText("");
        vista.dateFechaCompraVestido.setText("");
        vista.dateFechaEntregaVestido.setText("");
    }

    /**
     * Metodo que limpia los campos de diseñador
     */
    private void borrarCamposDiseniador() {
        vista.txtNombreDisen.setText("");
        vista.txtApellido.setText("");
        vista.dateFechaNac.setText("");
        vista.comboGenero.setSelectedItem(-1);
    }

    /**
     * Metodo que devuelve si los campos de diseñador estan vacios
     * @return
     */
    private boolean comprobarDiseniadorVacio(){
        return vista.txtNombreDisen.getText().isEmpty() ||
                vista.txtApellido.getText().isEmpty() ||
                vista.dateFechaNac.getText().isEmpty() ||
                vista.comboGenero.getSelectedIndex() == -1;
    }
    /**
     * Metodo que devuelve si los campos de traje estan vacios
     * @return
     */
    private boolean comprobarTrajeVacio(){
        return vista.txtNombreTraje.getText().isEmpty() ||
                vista.comboEstacionTraje.getSelectedIndex() == -1 ||
                vista.comboColorTraje.getSelectedIndex() == -1 ||
                vista.comboDiseniadorTraje.getSelectedIndex() == -1 ||
                vista.comboMarcaTraje.getSelectedIndex() == -1 ||
                vista.dateFechaCompraTraje.getText().isEmpty() ||
                vista.dateFechaEntregaTraje.getText().isEmpty();
    }
    /**
     * Metodo que devuelve si los campos de vestido estan vacios
     * @return
     */
    private boolean comprobarVestidoVacio(){
        return vista.txtNombreVestido.getText().isEmpty() ||
                vista.comboEstacionVestido.getSelectedIndex() == -1 ||
                vista.comboColorVestido.getSelectedIndex() == -1 ||
                vista.comboDiseniadorVestido.getSelectedIndex() == -1 ||
                vista.comboMarcaVestido.getSelectedIndex() == -1 ||
                vista.dateFechaCompraVestido.getText().isEmpty() ||
                vista.dateFechaEntregaVestido.getText().isEmpty();
    }

    @Override
    public void itemStateChanged(ItemEvent itemEvent) {

    }

    @Override
    public void windowOpened(WindowEvent windowEvent) {

    }

    @Override
    public void windowClosing(WindowEvent windowEvent) {

    }

    @Override
    public void windowClosed(WindowEvent windowEvent) {

    }

    @Override
    public void windowIconified(WindowEvent windowEvent) {

    }

    @Override
    public void windowDeiconified(WindowEvent windowEvent) {

    }

    @Override
    public void windowActivated(WindowEvent windowEvent) {

    }

    @Override
    public void windowDeactivated(WindowEvent windowEvent) {

    }

}
