package gui;

import javax.swing.*;
import java.awt.*;

public class OptionDialog extends JDialog{
    // Creamos los distintos elementos de la vista
     private JPanel panel1;
     JButton btnOpcionesGuardar;
     JPasswordField pfContraAdmin;
     JPasswordField pfContr;
     JTextField tfUsuario;
     JTextField tfIP;
    // Creamos la ventana
    private Frame owner;

    /**
     * Constructor de la clase con el super. Dandole un dueño, iniciando el owner y llamando al metodo que crea la ventana
     * @param owner
     */
    public OptionDialog(Frame owner){
        super(owner, "Opciones", true);
        this.owner=owner;
        initDialog();
    }

    /**
     * Crea la ventana del option Dialog
     */
    private void initDialog() {
        this.setContentPane(panel1);
        this.panel1.setBorder(BorderFactory.createEmptyBorder(20,20,20,20));
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        this.pack();
        this.setSize(new Dimension(this.getWidth()+200, this.getHeight()));
        this.setLocationRelativeTo(owner);
    }
}
