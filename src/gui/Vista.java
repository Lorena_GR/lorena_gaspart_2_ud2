package gui;

import base.enums.*;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

public class Vista extends JFrame{
    // Creamos el titulo del frame
    private final static String TITULOFRAME = "Aplicación varias tablas ropa de ceremonia";
    // Creamos los distintos paneles
    private JTabbedPane tabbedPane;
    private JPanel panel1;
    private JPanel JPanelVestido;
    private JPanel JPanelDise;
    private JPanel JPanelTraje;

    // Elementos del VESTIDO
     JTextField txtNombreVestido;
     JComboBox comboEstacionVestido;
     JComboBox comboColorVestido;
     JTextField txtPrecioVestido;
     DatePicker dateFechaCompraVestido;
     DatePicker dateFechaEntregaVestido;
     JSlider sliderTallaVestido;
     JButton btnAnyadirVestido;
     JButton btnModificarVestido;
     JButton btnEliminarVestido;
     JTable tablaVestido;
     JComboBox comboMarcaVestido;
     JComboBox comboDiseniadorVestido;

    // Elementos del TRAJE
     JButton btnAnyadirTraje;
     JButton btnModificarTraje;
     JButton btnEliminarTraje;
     JTable tablaTraje;
     JComboBox comboEstacionTraje;
     JTextField txtPrecioTraje;
     JTextField txtNombreTraje;
     JComboBox comboColorTraje;
     JSlider sliderTallaTraje;
     JComboBox comboMarcaTraje;
     JComboBox comboDiseniadorTraje;
     DatePicker dateFechaCompraTraje;
     DatePicker dateFechaEntregaTraje;

     // Elementos del DISEÑADOR
     JTextField txtNombreDisen;
     JTextField txtApellido;
     JComboBox comboGenero;
     JButton btnAnyadirDisen;
     JButton btnModificarDisen;
     JButton btnEliminarDisen;
     JTable tablaDisenador;
     DatePicker dateFechaNac;
     JButton btnBuscarDisen;

    // Elementos de BUSQUEDA
     JLabel etiquetaEstado;

     // DEFAULT TABLE MODEL
     DefaultTableModel dtmVestido;
     DefaultTableModel dtmTraje;
     DefaultTableModel dtmDisenador;

     //BARRA MENU
     JMenuItem itemOpciones;
     JMenuItem itemDesconectar;
     JMenuItem itemSalir;

     // CUADRO DIALOGO
     OptionDialog optionDialog;
     JDialog adminPasswordDialog;
     JButton btnValidate;
     JPasswordField adminPassword;

    /**
     * Metodo que llama al super poniendole el nombre a la vista
     * Llamando al metodo que crea el frame
     */
    public Vista(){
        super(TITULOFRAME);
        initFrame();
    }

    /**
     * Metodo que crea la ventana y llama a los distintos metodos
     */
    private void initFrame() {
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JDialog.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);
        this.setSize(new Dimension(this.getWidth()+200, this.getHeight()+100));
        this.setLocationRelativeTo(this);
        // Creo cuadro de dialogo
        optionDialog=new OptionDialog(this);
        setMenu();
        setAdminDialog();
        setEnumComboBox();
        setTableModels();
    }

    /**
     * Metodo en el que se crean los dtm y se les da el modelo a las tablas.
     * Un DefaultTableModel es un modelo generico que se usa cuando el programadro
     * no define ningun modelo de tabla.
     * Y el setModel sirve para darle un modelo a las tablas con un DefaultTableModel creado
     * anteriormente
     */
    private void setTableModels(){
        this.dtmDisenador = new DefaultTableModel();
        this.tablaDisenador.setModel(dtmDisenador);
        this.dtmTraje = new DefaultTableModel();
        this.tablaTraje.setModel(dtmTraje);
        this.dtmVestido = new DefaultTableModel();
        this.tablaVestido.setModel(dtmVestido);
    }

    /**
     * Metodo en el que inicias los botone les das un action command y los diferentes campos y luego creas la ventana.
     * Luego creas un objeto que guardara PasswordField y el boton y luego lo añades al un JOPtionPane
     */
    private void setAdminDialog() {
        btnValidate = new JButton("Validar");
        btnValidate.setActionCommand("abrirOpciones");
        adminPassword = new JPasswordField();
        adminPassword.setPreferredSize(new Dimension(100, 26));
        Object[] options = new Object[]{adminPassword, btnValidate};
        JOptionPane jop = new JOptionPane("Introduce la contraseña", JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION, null, options);
        adminPasswordDialog = new JDialog(this, "Opciones", true);
        adminPasswordDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        adminPasswordDialog.setContentPane(jop);
        adminPasswordDialog.pack();
        adminPasswordDialog.setLocationRelativeTo(this);
    }

    /**
     * Metodo en el que creas un menu y sus distintos items y les das un nombre que se usara a la hora de llamarlos
     * Luego se añaden al menu y luego se añade el menu a la barra de la ventana
     */
    private void setMenu() {
        JMenuBar mbBar = new JMenuBar();
        JMenu menu = new JMenu("Archivo");
        // Por cada item necesita un actioncomand
        itemOpciones = new JMenuItem("Opciones");
        itemOpciones.setActionCommand("Opciones");
        itemDesconectar = new JMenuItem("Desconectar");
        itemDesconectar.setActionCommand("Desconectar");
        itemSalir = new JMenuItem("Salir");
        itemSalir.setActionCommand("Salir");
        menu.add(itemOpciones);
        menu.add(itemDesconectar);
        menu.add(itemSalir);
        mbBar.add(menu);
        // Centrar en horizonatl
        mbBar.add(Box.createHorizontalGlue());
        this.setJMenuBar(mbBar);
    }

    /**
     * Aqui se leeran las distintas enumeraciones con un foreach que recorra los valores y por cada valor se iran añadiendo al comboBox deseado.
     */
    private void setEnumComboBox(){
        for(Colores constant: Colores.values()){
            comboColorTraje.addItem(constant.getColor());
            comboColorVestido.addItem(constant.getColor());
        }
        for(Estaciones constant : Estaciones.values()){
            comboEstacionTraje.addItem(constant.getEstacion());
            comboEstacionVestido.addItem(constant.getEstacion());
        }
        for(MarcasVestidos constant : MarcasVestidos.values()){
            comboMarcaVestido.addItem(constant.getMarca());
        }
        for(MarcasTrajes constant : MarcasTrajes.values()){
            comboMarcaTraje.addItem(constant.getMarca());
        }
        for(Genero constant : Genero.values()){
            comboGenero.addItem(constant.getGenero());
        }
    }
}
