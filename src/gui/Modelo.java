package gui;

import java.io.*;
import java.sql.*;
import java.time.LocalDate;
import java.util.Properties;

public class Modelo {
    /**
     * Crea las diferentes variables
     */
    private String ip;
    private String user;
    private String password;
    private String adminPassword;

    public String getIp() {
        return ip;
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }

    public String getAdminPassword() {
        return adminPassword;
    }

    public Modelo(){
        getPropValues();
    }


    private Connection conexion;

    /**
     * Metodo que conectara con la base de datos
     */
    void conectar(){
        try {
            conexion = DriverManager.getConnection("jdbc:mysql://"+ ip + ":3306/ropa_ceremonia", user, password);
        }catch (SQLException e){
            try{
                conexion = DriverManager.getConnection("jdbc:mysql://"+ ip + ":3306/", user, password);
                PreparedStatement statement = null;
                String code = leerFichero();
                // Cuando lea el fichero si contiene los dos guiones los quitara y guardara lo escrito hasta ahí como un elemento del vector
                String[] query = code.split("--");
                for (String aQuery : query){
                    statement = conexion.prepareStatement(aQuery);
                    statement.executeUpdate();
                }
                assert statement != null;
                statement.close();
            } catch (SQLException | IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    /**
     * Metodo que leera un fichero ya creado con la base de datos y leera linea a linea
     * @return
     * @throws IOException
     */
    private String leerFichero() throws IOException {
        try(BufferedReader reader = new BufferedReader(new FileReader("basedatos_java.sql"))){
            String linea;
            StringBuilder stringBuilder = new StringBuilder();
            while ((linea = reader.readLine())!=null){
                stringBuilder.append(linea);
                stringBuilder.append(" ");
            }
            // returna una cadena de caracters mutable convertida a un string
            return stringBuilder.toString();
        }
    }

    /**
     * Metodo que cerrara la conexion.
     */
    void desconectar(){
        try {
            conexion.close();
            conexion = null;
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo que dado diferentes valores y con una sentencia añade una linea a la tabla diseñador
     * @param nombre
     * @param apellido
     * @param fechaNacimiento
     * @param genero
     */
    void insertarDisenador(String nombre, String apellido, LocalDate fechaNacimiento, String genero){
        String sentenciaSql = "INSERT INTO diseniador (nombre, apellido, fechanacimiento, genero)" + "VALUES(?,?,?,?)";
        // Nos permite preparar una sentencia sql
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, apellido);
            sentencia.setDate(3, Date.valueOf(fechaNacimiento));
            sentencia.setString(4, genero);
            sentencia.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if(sentencia != null){
                try{
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Metodo para insertar un vestido
     * @param nombre
     * @param estacion
     * @param color
     * @param talla
     * @param diseniador
     * @param marca
     * @param precio
     * @param fechaCompra
     * @param fechaEntrega
     */
    void insertarVestido(String nombre, String estacion, String color, int talla, String diseniador, String marca,
     float precio, LocalDate fechaCompra, LocalDate fechaEntrega){
        String sentenciaSql = "INSERT INTO vestido (nombre, estacion, color, talla, iddiseniador, marca, precio, fechacompra, fechaentrega)"
                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement sentencia = null;

        int iddiseniador = Integer.valueOf(diseniador.split(" ")[0]);

        try {
            sentencia=conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, estacion);
            sentencia.setString(3, color);
            sentencia.setInt(4, talla);
            sentencia.setInt(5, iddiseniador);
            sentencia.setString(6, marca);
            sentencia.setFloat(7, precio);
            sentencia.setDate(8, Date.valueOf(fechaCompra));
            sentencia.setDate(9, Date.valueOf(fechaEntrega));
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if(sentencia!=null){
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    /**
     * Metodo para insertar un traje
     * @param nombre
     * @param estacion
     * @param color
     * @param talla
     * @param diseniador
     * @param marca
     * @param precio
     * @param fechaCompra
     * @param fechaEntrega
     */
    void insertarTraje(String nombre, String estacion, String color, int talla, String diseniador, String marca,
                       float precio, LocalDate fechaCompra, LocalDate fechaEntrega){
        String sentenciaSql = "INSERT INTO traje (nombre, estacion, color, talla, iddiseniador, marca, precio, fechacompra, fechaentrega)"
                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement sentencia = null;

        int iddiseniador = Integer.valueOf(diseniador.split(" ")[0]);

        try {
            sentencia=conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, estacion);
            sentencia.setString(3, color);
            sentencia.setInt(4, talla);
            sentencia.setInt(5, iddiseniador);
            sentencia.setString(6, marca);
            sentencia.setFloat(7, precio);
            sentencia.setDate(8, Date.valueOf(fechaCompra));
            sentencia.setDate(9, Date.valueOf(fechaEntrega));
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if(sentencia!=null){
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Metodo para modificar un diseñador segun una sentencia sql
     * @param nombre
     * @param apellido
     * @param fechaNacimiento
     * @param genero
     * @param idDiseniador
     */
    void modificarDisenador(String nombre, String apellido, LocalDate fechaNacimiento, String genero, int idDiseniador){
        String sentenciaSql = "UPDATE diseniador SET nombre=?, apellido=?, fechanacimiento=?, genero=?" + "WHERE iddiseniador=?";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, apellido);
            sentencia.setDate(3, Date.valueOf(fechaNacimiento));
            sentencia.setString(4, genero);
            sentencia.setInt(5, idDiseniador);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (sentencia!=null){
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Metodo para modificar un vestido según una sentencia sql
     * @param nombre
     * @param estacion
     * @param color
     * @param talla
     * @param disenador
     * @param marca
     * @param precio
     * @param fechaCompra
     * @param fechaEntrega
     * @param idvestido
     */
    void modificarVestido(String nombre, String estacion, String color, int talla, String disenador, String marca,
                          float precio, LocalDate fechaCompra, LocalDate fechaEntrega, int idvestido){
        String sentenciaSql = "UPDATE vestido SET nombre=?, estacion=?, color=?, talla=?, iddiseniador=?, marca=?, precio=?, fechacompra=?, fechaentrega=?"+ "WHERE idvestido=?";
        PreparedStatement sentencia = null;
        int iddiseniador = Integer.valueOf(disenador.split(" ")[0]);
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, estacion);
            sentencia.setString(3, color);
            sentencia.setInt(4, talla);
            sentencia.setInt(5, iddiseniador);
            sentencia.setString(6, marca);
            sentencia.setFloat(7, precio);
            sentencia.setDate(8, Date.valueOf(fechaCompra));
            sentencia.setDate(9, Date.valueOf(fechaEntrega));
            sentencia.setInt(10, idvestido);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (sentencia!=null){
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    /**
     * Metodo para modificar un traje según una sentencia sql
     * @param nombre
     * @param estacion
     * @param color
     * @param talla
     * @param diseniador
     * @param marca
     * @param precio
     * @param fechaCompra
     * @param fechaEntrega
     * @param idtraje
     */
    void modificarTraje(String nombre, String estacion, String color, int talla, String diseniador, String marca,
                        float precio, LocalDate fechaCompra, LocalDate fechaEntrega, int idtraje){
        String sentenciaSql = "UPDATE traje SET nombre=?, estacion=?, color=?, talla=?, iddiseniador=?, marca=?, precio=?, fechacompra=?, fechaentrega=?"+ " WHERE idtraje=?";
        PreparedStatement sentencia = null;
        int iddiseniador = Integer.valueOf(diseniador.split(" ")[0]);
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, estacion);
            sentencia.setString(3, color);
            sentencia.setInt(4, talla);
            sentencia.setInt(5, iddiseniador);
            sentencia.setString(6, marca);
            sentencia.setFloat(7, precio);
            sentencia.setDate(8, Date.valueOf(fechaCompra));
            sentencia.setDate(9, Date.valueOf(fechaEntrega));
            sentencia.setInt(10, idtraje);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (sentencia!=null){
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    /**
     * Metodo que eliminar un diseniador segun el id seleccionado
     * @param iddiseniador
     */
    void eliminarDisenador(int iddiseniador){
        String sentenciaSql = "DELETE FROM diseniador WHERE iddiseniador=?";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, iddiseniador);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (sentencia!=null){
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    /**
     * METODO QUE ELIMINA UN VESTIDO SEGUN EL NOMBRE SELECCIONADO
     */
    void eliminarVestido(String nombre){
        String sentenciaSql = "DELETE FROM vestido WHERE nombre=?";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (sentencia!=null){
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    /**
     * METODO QUE ELIMINA UN TRAJE SEGUN EL NOMBRE SELECCIONADO
     */
    void eliminarTraje(String nombre){
        String sentenciaSql = "DELETE FROM traje WHERE nombre=?";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (sentencia!=null){
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    /**
     * METODO QUE BUSCA UN DISEÑADOR SEGUN EL NOMBRE SELECCIONADO
     */
    void buscarDiseniador(String nombre){
        String sentenciaSql = "SELECT * FROM disenador WHERE nombre=?";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo que le un archivo y agarra las distintas propiedades de este y las guarda en una variable
     */
    private void getPropValues() {
        InputStream inputStream = null;

        try {
            Properties prop = new Properties();
            String propFileName = "config.properties";
            inputStream = new FileInputStream(propFileName);
            prop.load(inputStream);
            ip = prop.getProperty("ip");
            user = prop.getProperty("user");
            password = prop.getProperty("pass");
            adminPassword = prop.getProperty("admin");
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if (inputStream !=null){
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Metodo que guarda en un archivo las propiedades que anteriormente hemos escrito.
     * @param ip
     * @param user
     * @param pass
     * @param adminPass
     */
    public void setPropValues(String ip, String user, String pass, String adminPass){
        try {
            Properties prop = new Properties();
            prop.setProperty("ip", ip);
            prop.setProperty("user", user);
            prop.setProperty("pass", pass);
            prop.setProperty("admin", adminPass);
            OutputStream out = new FileOutputStream("config.properties");
            prop.store(out, null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.ip = ip;
        this.user = user;
        this.password = pass;
        this.adminPassword = adminPass;
    }

    /**
     * Metodo que consulta diseñador
     * @return
     * @throws SQLException
     */
    ResultSet consultarDiseniador() throws SQLException {
        String sentenciaSql = "SELECT concat(iddiseniador) AS 'ID', concat(nombre) AS 'Nombre', concat(apellido) AS 'Apellido'" +
                ", concat(fechanacimiento) AS 'Fecha de nacimiento', concat(genero) AS 'Genero' from diseniador";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;

    }

    /**
     * Metodo que consulta vestido
     * @return
     * @throws SQLException
     */
    ResultSet consultarVestido() throws SQLException{
        String sentenciaSql = "SELECT concat(idvestido) AS 'ID', concat(nombre) AS 'Nombre', concat(estacion) AS 'Estacion'" +
                ", concat(color) AS 'Color', concat(talla) AS 'Talla', concat(iddiseniador) AS 'Diseñador', concat(marca) AS 'Marca'" +
                ", concat(precio) AS 'Precio', concat(fechacompra) as 'Fecha de compra', concat(fechaentrega) AS 'Fecha de entrega' from vestido";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     * Metodo que consulta traje
     * @return
     * @throws SQLException
     */
    ResultSet consultarTraje() throws SQLException {
        String sentenciaSql = "SELECT concat(idtraje) AS 'ID', concat(nombre) AS 'Nombre', concat(estacion) AS 'Estacion'" +
                ", concat(color) AS 'Color', concat(talla) AS 'Talla', concat(iddiseniador) AS 'Diseñador', concat(marca) AS 'Marca'" +
                ", concat(precio) AS 'Precio', concat(fechacompra) as 'Fecha de compra', concat(fechaentrega) AS 'Fecha de entrega' from traje";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;

    }

    /**
     * Metodo que agarra la función creada en la base de datos
     * @param nombre
     * @return
     */
    public boolean nombreVestidoYaExiste(String nombre){
        String nombreVestido = "SELECT existeNombreVestido(?)";
        PreparedStatement funcion;
        boolean existe = false;
        try {
            funcion = conexion.prepareCall(nombreVestido);
            funcion.setString(1, nombre);
            ResultSet set = funcion.executeQuery();
            set.next();
            existe = set.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return existe;
    }
    /**
     * Metodo que agarra la función creada en la base de datos
     * @param nombre
     * @return
     */
    public boolean nombreTrajeYaExiste(String nombre){
        String nombreTraje = "SELECT existeNombreTraje(?)";
        PreparedStatement funcion;
        boolean existe = false;
        try {
            funcion = conexion.prepareCall(nombreTraje);
            funcion.setString(1, nombre);
            ResultSet set = funcion.executeQuery();
            set.next();
            existe = set.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return existe;
    }
    /**
     * Metodo que agarra la función creada en la base de datos
     * @param nombre
     * @return
     */
    public boolean disenadorYaExiste(String nombre, String apellido){
        String nombreCompleto = apellido + ", " + nombre;
        String authorNameConsult = "SELECT existeNombreDiseniador(?)";
        PreparedStatement funcion;
        boolean existe = false;
        try {
            funcion = conexion.prepareCall(authorNameConsult);
            funcion.setString(1, nombreCompleto);
            ResultSet set = funcion.executeQuery();
            set.next();
            existe = set.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return existe;

    }
}
