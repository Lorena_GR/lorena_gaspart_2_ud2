package main;

import gui.Controlador;
import gui.Modelo;
import gui.Vista;

public class Principal {
    /**
     * Clase principal que crea la vista, el modelo y el controlador.
     * @param args
     */
    public static void main(String[] args) {
        Vista vista = new Vista();
        Modelo modelo = new Modelo();
        Controlador controlador = new Controlador(vista, modelo);
    }
}
